"""
This code has been developped by P. Malisani in 2022 for the paper
"Primal-Dual Interior Point Methods for State and Input Constraint Optimal Control: Theoritical Results and Solving Algorithm"
submitted to SIAM Journal on Control and Optimization
"""

import numpy as np
import casadi as cs
import matplotlib.pyplot as plt


def fischer_burmeister(lbd, g, eps):
    """
    :param lbd: Dual variables (lbd >0)
    :param g: Contrainst (g >0)
    :param eps: Penalty parameter
    :return: Fischer Burmeister complementarity regularization function
    """
    return lbd + g - np.sqrt(lbd ** 2 + g ** 2 + 2. * eps)


def plot_sequence(time, seq, name, title):
    """
    This function is used for data vizualization only
    """
    plt.figure()
    for i, y in enumerate(seq):
        if i < len(seq)-1:
            width = 2
            style = "dashed"
            plt.plot(time, y, label="$\epsilon$ = 1e-" + str(i + 1), linestyle=style, linewidth=width)
        else:
            color = "black"
            width = 3
            style = "solid"
            plt.plot(time, y, label="$\epsilon$ = 1e-" + str(i + 1), color=color, linestyle=style, linewidth=width)
    plt.suptitle(title, fontsize=24)
    plt.xlabel('time', fontsize=20)
    plt.ylabel(name, fontsize=20)
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    plt.legend(loc="upper right", fontsize=20)


class VanderPol:
    def __init__(self,):
        self.a = -.4
        self.eps = 1.
        time, xp_init, z_init = self.initialize()
        self.prb, self.xp, self.z, self.time, self.eps = self._build_casadi_problem(time, xp_init, z_init)

    def initialize(self):
        """
        This method initializes the solution of the problem
        """
        time = np.linspace(0., 4., 201)
        xp = np.zeros((4, len(time)))
        xp[0, :] = 1.
        xp[1, :] = 1.
        z = np.ones((4, len(time)))
        return time, xp, z

    def set_eps(self, eps):
        """
        This method allows to change the value of the penalty parameter in the CASADI Opti instanciation
        """
        self.prb.set_value(self.eps, eps)

    def state_constraint(self, x, dx=0):
        """
        This method returns the state constraints when dx=0 and its derivative when dx=1
        :param x: x2(t)
        :param dx: order of the derivative of the state constraint a - x_2(t) wrt x2
        """
        if dx == 0:
            return self.a - x[1]
        return -1.

    def ode(self, time, xp, z):
        """
        rhs of the system dynamics written in casadi format
        """
        x = xp[:2]
        p = xp[2:]
        u = z[0]
        mux = z[1]
        f0 = x[1]
        f1 = - x[0] + x[1] * (1. - x[0] ** 2) + u
        f2 = - 2. * x[0] + p[1] + 2. * p[1] * x[0] * x[1]
        f3 = - 2. * x[1] - p[0] - p[1] * (1. - x[0] ** 2) - mux * self.state_constraint(x, dx=1)
        return cs.vertcat(f0, f1, f2, f3)

    def algeq(self, time, xp, z, eps):
        """
        rhs of algebraic equations from the primal dual algorithm written in casadi format
        """
        x = xp[:2]
        p = xp[2:]
        u = z[0]
        mux, nup, num = z[1], z[2], z[3]
        g0 = 2. * u + p[1] + nup - num
        g1 = fischer_burmeister(mux, -self.state_constraint(x), eps)
        g2 = fischer_burmeister(nup, 1. - u, eps)
        g3 = fischer_burmeister(num, u + 1, eps)
        return cs.vertcat(g0, g1, g2, g3)

    def twobc(self, xp0, xpT, z0, zT):
        """
        Boundary conditions of the problem
        """
        return cs.vertcat(xp0[0] - 1., xp0[1] - 1., xpT[2] - 10. * xpT[0], xpT[3] - 10. * xpT[1])

    def _build_casadi_problem(self, time, xp_init, z_init):
        """
        This method allows to build the casadi problem corresponding to the TPBVP of DAEs solution of the primal dual
        IPM Algorithm.
        The discretization scheme of the ODEs is a 3 stages Lobatto IIIA method, aka Hermite-simpson method.
        """
        prb = cs.Opti()
        xp = prb.variable(xp_init.shape[0], xp_init.shape[1])
        z = prb.variable(z_init.shape[0], z_init.shape[1])
        eps = prb.parameter()
        prb.set_initial(xp, xp_init)
        prb.set_initial(z, z_init)
        method = "hermite_simpson"
        for k in range(len(time)-1):
            if method == "bogacki":
                h = time[k + 1] - time[k]
                zp05 = .5 * (z[:, k] + z[:, k + 1])
                zp75 = .25 * z[:, k] + .75 * z[:, k + 1]
                k1 = self.ode(time[k], xp[:, k], z[:, k])
                k2 = self.ode(time[k] + h / 2., xp[:, k] + .5 * h * k1, zp05)
                k3 = self.ode(time[k] + h * .75, xp[:, k] + .75 * h * k2, zp75)
                xkp1 = xp[:, k] + h * (2. / 9. * k1 + 1. / 3. * k2 + 4. / 9. * k3)
                prb.subject_to(xp[:, k + 1] == xkp1)
            else:
                rhsk = self.ode(time[k], xp[:, k], z[:, k])
                rhskp1 = self.ode(time[k + 1], xp[:, k + 1], z[:, k + 1])
                xp05 = .5 * (xp[:, k] + xp[:, k + 1]) - .125 * (time[k + 1] - time[k]) * (rhskp1 - rhsk)
                zp05 = .5 * (z[:, k] + z[:, k + 1])
                tp05 = (time[k + 1] + time[k]) * .5
                rhskp05 = self.ode(tp05, xp05, zp05)
                xkp1 = xp[:, k] + (rhskp1 + 4. * rhskp05 + rhsk) * (time[k + 1] - time[k]) / 6.
                prb.subject_to(xp[:, k + 1] == xkp1)
            alg_constraints = self.algeq(time[k], xp[:, k], z[:, k], eps)
            prb.subject_to(alg_constraints == 0.)

        alg_constraints = self.algeq(time[-1], xp[:, -1], z[:, -1], eps)
        prb.subject_to(alg_constraints == 0.)
        bc = self.twobc(xp[:, 0], xp[:, -1], z[:, 0], z[:, -1])
        prb.subject_to(bc == 0.)
        opts = {'ipopt.print_level': 0, 'print_time': 0, 'ipopt.sb': 'yes'}
        prb.solver("ipopt", opts)
        return prb, xp, z, time, eps

    def solve(self, eps, xp_init=None, z_init=None):
        """
        This method updates the penalty parameter and solve the TPBVP
        """
        self.set_eps(eps)
        if xp_init is not None and z_init is not None:
            self.prb.set_initial(self.xp, xp_init)
            self.prb.set_initial(self.z, z_init)
        sol = self.prb.solve()
        xp_val = sol.value(self.xp)
        z_val = sol.value(self.z)
        return self.time, xp_val, z_val


alpha = .1
eps = 1e-1
x1s, x2s, p1s, p2s, us, muxs, nups, nums = list(), list(), list(), list(), list(), list(), list(), list()
xp_init, z_init = None, None
vdp_prb = VanderPol()

while eps >= 1e-7:
    print("epsilon = ", eps)
    time, xp, z = vdp_prb.solve(eps, xp_init=xp_init, z_init=z_init)
    eps *= alpha
    xp_init = xp
    z_init = z
    x1s.append(xp[0, :])
    x2s.append(xp[1, :])
    p1s.append(xp[2, :])
    p2s.append(xp[3, :])
    us.append(z[0, :])
    muxs.append(z[1, :])
    nups.append(z[2, :])
    nums.append(z[3, :])

plot_sequence(time, x1s, "$x_1(t)$", "Sequence of penalized optimal $x_1$")
plot_sequence(time, x2s, "$x_2(t)$", "Sequence of penalized optimal $x_2$")
plot_sequence(time, p1s, "$p_1(t)$", "Sequence of penalized optimal $p_1$")
plot_sequence(time, p2s, "$p_2(t)$", "Sequence of penalized optimal $p_2$")
plot_sequence(time, us, "$u(t)$", "Sequence of penalized optimal $u$")
plot_sequence(time, muxs, "$\lambda(t)$", "Sequence of penalized optimal $\lambda$")
plot_sequence(time, nups, "$\\nu^+(t)$", "Sequence of penalized optimal $\\nu^+$")
plot_sequence(time, nums, "$\\nu^-(t)$", "Sequence of penalized optimal $\\nu^-$")
plt.show()



